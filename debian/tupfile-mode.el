;;; tupfile-mode.el --- Major mode for editing Tupfile   -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Dmitry Bogatov

;; Author: Dmitry Bogatov <KAction@gnu.org>
;; Keywords: maint, unix

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defmacro tupfile--inherit-face (face parent docstring)
  (declare (doc-string 2) (indent 2))
  "Declare new FACE with DOCSTRING, inheriting PARENT."
  (let ((facename (intern (format "tupfile-%s-face" face))))
    `(progn
       (defvar ,facename ',facename ,docstring)
       (defface ,facename
	 '((t :inherit ,parent))
	 ,docstring
	 :group 'tupfile))))

(tupfile--inherit-face comment font-lock-comment-face
  "Face used to highlight Tupfile comments.")
(tupfile--inherit-face config-variable font-lock-warning-face
  "Face used to highlight config variables.")
(tupfile--inherit-face general-variable font-lock-variable-name-face
  "Face used to highlight general, read-write variables.")
(tupfile--inherit-face control font-lock-preprocessor-face
  "Face used to highlight control structures.")
(tupfile--inherit-face command font-lock-string-face
  "Face used to highlight commands in rules.")
(tupfile--inherit-face separator font-lock-doc-face
  "Face used to highlight separators (|>).")
(tupfile--inherit-face keyword font-lock-keyword-face
  "Face used to highlight the only keyword (foreach).")
(tupfile--inherit-face %-flags font-lock-type-face
  "Face used to highlight %-flags.")
(tupfile--inherit-face group font-lock-constant-face
  "Face used to highlight references to groups.")
(tupfile--inherit-face bin font-lock-function-name-face
  "Face used to highlight references to bins.")
(tupfile--inherit-face bang font-lock-negation-char-face
  "Face used to highlight bang macroses.")

(defvar tupfile--font-lock-keywords
  (list
   '("^ *#.*" . tupfile-comment-face)
   '("\\(|>\\)\\(.*\\)\\(|>\\)"
     (1 tupfile-separator-face)
     (2 tupfile-command-face)
     (3 tupfile-separator-face))
   '("\\(@([-_A-Z]+)\\)"  (1 tupfile-config-variable-face t))
   '("\\(\\$(CONFIG_[-._A-Za-z]+)\\)" (1 tupfile-config-variable-face t))
   '("\\(\\$([-_.%A-Za-z]+)\\)" (1 tupfile-general-variable-face t))
   '("^\\(?:ifeq\\|ifdef\\|ifndef\\|else\\|endif\\|include\\|run\\|preload\\)"
     . tupfile-control-face)
   '("^ *\\([-_.A-Za-z]+\\) *="  (1 tupfile-general-variable-face))
   '("|>?" . tupfile-separator-face)
   '("^ *:" . tupfile-separator-face)
   '("^ *: +\\(foreach\\) " (1 tupfile-keyword-face))
   '("\\(%[fbBeOodg]\\)" (1 tupfile-%-flags-face t))
   '("\\(%<\\)\\([a-z]+\\)\\(>\\)"
     (1 tupfile-%-flags-face)
     (2 tupfile-group-face t)
     (3 tupfile-%-flags-face))
   '("<[a-z]+>" . tupfile-group-face)
   '("{[a-z]+}" . tupfile-bin-face)
   '("\\(![0-9a-z]+\\)" (1 tupfile-bang-face t))
   ))

;;;###autoload
(add-to-list 'auto-mode-alist '("Tupfile" . tupfile-mode))

;;;###autoload
(define-derived-mode tupfile-mode prog-mode "tup"
  "Major mode for edititng tup build system rules.

\\<tupfile-mode-map>"
  (setq font-lock-defaults '(tupfile--font-lock-keywords)))

(provide 'tupfile-mode)
;;; tupfile-mode.el ends here
